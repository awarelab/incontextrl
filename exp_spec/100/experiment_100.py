from mrunner.helpers.specification_helper import create_experiments_helper

EXP_NAME = "test"

experiments_list = create_experiments_helper(
    experiment_name=EXP_NAME,
    # just for development. Loosely based on examples/serve_llama_7b.sh but smaller
    base_config={
        "InContextLearner.max_context": 1000,
    },
    params_grid={},
    exclude=[
        ".neptune",
        ".idea",
        ".git",
        "__pychache__",
    ],
    python_path="",
    script=f"source /net/tscratch/people/plgloss/env/bin/activate; python3 main.py",
    tags=[globals()["script"][:-3]], with_neptune=True,
    project_name="pmtest/incotextrl",
    exclude_git_files=False
)
