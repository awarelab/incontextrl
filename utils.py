import neptune
import os


class NeptuneLogger:
    def __init__(self, params):
        self.run = neptune.init_run(
            project="pmtest/incotextrl",
            api_token=os.environ["NEPTUNE_API_TOKEN"],
        )
        for k, v in params.items():
            self.run[k] = v

    def log(self, key, value, overwrite=False):
        if overwrite:
            self.run[key] = value
        else:
            self.run[key].append(value)