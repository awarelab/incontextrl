import numpy as np
import sys
import gin

@gin.configurable
class InContextLearner:
    def __init__(self, env, llm, init_episodes=100, max_episodes=200, max_context=10, logger=None):
        self.env = env
        self.llm = llm
        self.init_episodes = init_episodes
        self.max_episodes = max_episodes
        self.max_context = max_context
        self.logger = logger
        print('InContextLearner.max_context', self.max_context)
        self.episodes = []
        self.rewards = []


    def run(self):
        env = self.env
        episodes = self.episodes
        rewards = self.rewards

        # Generate some random policy rollouts and add them to memory.
        while len(episodes) < self.init_episodes:
          episode = []
          s = self.env.reset()
          while not env.terminated:
            a = env.random_act()
            episode.append((s, a))
            s = env.step(a)
          episodes.append(episode)
          rewards.append(env.reward)

        # Incremental rollouts with the LLM in the loop.
        while len(episodes) < self.max_episodes:

          context = self.build_context()
          state = env.reset()
          buffer = []

          # Set a desired reward for the current rollout.
          desired_reward = np.max(rewards) + 20 + np.int32(np.random.uniform() * 10)
          prompt = f"{desired_reward}:"
          print(f"Desired reward: {desired_reward}.")

          while not env.terminated and env.reward < 200:
            state_prompt = f"{env.state_to_str(state)},"
            prompt += state_prompt
            # sys.stdout.write(state_prompt)

            # LLM inference.
            ll = len(context + prompt)
            pred = self.llm(context + prompt)

            # If predicted action is invalid, sample random action.
            # Alternatively, one can sample the LLM from only the set of valid actions
            # by using a logit bias. This is the approach we take in our experiments.
            try:
              act = env.str_to_act(pred.strip())
            except:
              act = -1
            if act not in [0, 1]:
              print(f"Invalid action '{pred}'. Sampling random one.")
              act = env.random_act()

            action_prompt = f"{env.act_to_str(act)},"
            prompt += action_prompt
            # sys.stdout.write(action_prompt)
            buffer.append((state, act))



            # Step environment.
            state = env.step(act)

          print("\n Achieved: ", env.reward)
          episodes.append(buffer)
          rewards.append(env.reward)

          self.logger.log('reward', env.reward)
          self.logger.log('max reward', np.max(rewards))

          self.show_progress_as_img()




    def show_progress_as_img(self):
        import matplotlib.pyplot as plt
        # Make a plot of performance over time.
        rewards = self.rewards
        init_episodes = self.init_episodes
        plt.scatter(np.arange(init_episodes), rewards[:init_episodes], c="gray", alpha=0.3)
        plt.scatter(np.arange(init_episodes, len(rewards)), rewards[init_episodes:], alpha=0.3)
        max_over_time = [rewards[init_episodes]]
        for reward in rewards[init_episodes+1:]:
          max_over_time.append(max(reward, max_over_time[-1]))
        plt.plot(np.arange(init_episodes, len(rewards)), max_over_time)
        plt.axhline(y=200, color='gray', linestyle='--', alpha=0.3)
        # plt.show()
        # plt.savefig('progress.png')
        # get plt figure
        fig = plt.gcf()

        from neptune.types import File
        self.logger.log('images', File.as_image(fig), overwrite=True)

    def build_context(self):
        # Build context of episodes sorted by ascending rewards.
        context = ""
        num_tokens = 0
        for i in np.argsort(self.rewards)[::-1]:
            if num_tokens + 10 > self.max_context:  # Each episode should have at least 10 tokens.
                break
            episode, reward = self.episodes[i], self.rewards[i]
            size = min(len(episode), (self.max_context - num_tokens) // 5)
            text = f"{reward}:" + ",".join([f"{self.env.state_to_str(s)},{self.env.act_to_str(a)}" for s, a in episode[:size]])
            num_tokens += 2 + size * 5  # Manual math here to count tokens. Calling the tokenizer too much can get slow.
            context = f"{text}\n{context}"

        print("---------------------------------------------------------")
        print("Num episodes:", len(self.episodes), "Curr highest return:", np.max(self.rewards))
        print("Context:")
        print(context)
        print("---------------------------------------------------------")
        return context