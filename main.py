from envs import CartPoleEnv
from learner import InContextLearner
from llms import FakeLLM, LongLLamaLLM
from utils import NeptuneLogger
from mrunner.helpers.client_helper import get_configuration


if __name__ == "__main__":
    params = get_configuration(with_neptune=False, inject_parameters_to_gin=True)
    env = CartPoleEnv()
    #llm = FakeLLM(["1", "2"])
    llm = LongLLamaLLM(temperature=0.0)
    logger = NeptuneLogger(params)

    InContextLearner(env, llm, logger=logger).run()

