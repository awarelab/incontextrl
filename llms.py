import numpy as np
import torch
from transformers import LlamaTokenizer, AutoModelForCausalLM

class LongLLamaLLM:
    def __init__(self, temperature):
        MODEL_PATH = 'syzymon/long_llama_3b'
        TOKENIZER_PATH = 'syzymon/long_llama_3b'
        # to fit into colab GPU we will use reduced precision
        TORCH_DTYPE = torch.bfloat16

        self.temperature = temperature

        if torch.cuda.is_available():
            self.device = torch.device("cuda")
        else:
            self.device = torch.device("cpu")
        print(self.device)

        self.tokenizer = LlamaTokenizer.from_pretrained(TOKENIZER_PATH)

        self.model = AutoModelForCausalLM.from_pretrained(MODEL_PATH,
                                                     torch_dtype=TORCH_DTYPE,
                                                     device_map=self.device,
                                                     trust_remote_code=True,
                                                     # mem_attention_grouping is used
                                                     # to trade speed for memory usage
                                                     # for details see the section Additional configuration
                                                     mem_attention_grouping=(1, 2048))
        self.model.eval()

    def __call__(self, *args, **kwargs):
        input_ids = self.tokenizer(args[0], return_tensors="pt").input_ids
        input_ids = input_ids.to(self.device)
        generation_output = self.model.generate(input_ids=input_ids, max_new_tokens=2, num_beams=1,
                                                temperature=self.temperature)

        input_size = input_ids[0].shape

        xyz = self.tokenizer.decode(generation_output[0, input_size[0]:])
        return xyz


class FakeLLM:

    def __init__(self, action_space):
        self.action_space = action_space

    def __call__(self, *args, **kwargs):
        return np.random.choice(self.action_space)