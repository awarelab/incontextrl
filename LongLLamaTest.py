import time
import sys
import gymnasium as gym
import matplotlib.pyplot as plt
import numpy as np
import openai
# from transformers import GPT2Tokenizer
import tiktoken

import torch
from transformers import LlamaTokenizer, AutoModelForCausalLM

MODEL_PATH = 'syzymon/long_llama_3b'
TOKENIZER_PATH = 'syzymon/long_llama_3b'
# to fit into colab GPU we will use reduced precision
TORCH_DTYPE = torch.bfloat16

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")


tokenizer = LlamaTokenizer.from_pretrained(TOKENIZER_PATH)

model = AutoModelForCausalLM.from_pretrained(MODEL_PATH,
                                            torch_dtype=TORCH_DTYPE,
                                            device_map=device,
                                            trust_remote_code=True,
                                            # mem_attention_grouping is used
                                            # to trade speed for memory usage
                                            # for details see the section Additional configuration
                                            mem_attention_grouping=(1, 2048))
model.eval()

from transformers import TextStreamer
streamer = TextStreamer(tokenizer)

prompt = "My name is Julien and I like to"
input_ids = tokenizer(prompt, return_tensors="pt").input_ids
input_ids = input_ids.to(device)




# input_ids = tokenizer(prompt, return_tensors="pt").input_ids
# input_ids = input_ids.to(device)

generation_output = model.generate(input_ids=input_ids, max_new_tokens=2, num_beams=1, temperature=0)

input_size = input_ids[0].shape

xyz = tokenizer.decode(generation_output[0, input_size[0]:])
print(xyz)
#
#
# print(LLM("Today we have a lovely day, ", max_tokens=4))
#
#
# torch.manual_seed(60)
# time_now = time.time()
#
# generation_output = model.generate(
#     input_ids=input_ids,
#     max_new_tokens=4,
#     num_beams=1,
#     last_context_length=1792,
#     do_sample=True,
#     temperature=1.0,
# )
# delta = time.time() - time_now
# print(delta, " ", delta/256)